/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareasmaps;

import java.util.Map;
import java.util.TreeMap;
import static org.daw1.tareasmaps.TareasMaps.teclado;

/**
 *
 * @author rgcenteno
 */
public class PalabrasContienen {
    
    public static void programa5(){
        String texto = "";
        do{
            System.out.println("Por favor inserte la frase a analizar");
            texto = teclado.nextLine().toLowerCase();
        }
        while(texto.isEmpty());
        texto = texto.replaceAll("[^\\sA-Za-z0-9]", "");//Limpieza
                
        System.out.println(mapToString(contarPalabrasUnicasContienen(texto)));
    }
    
    private static Map<Character,java.util.Set<String>> contarPalabrasUnicasContienen(String texto){
        String[] palabras = texto.split("\\s+");
        
        java.util.Set<String> palabrasProcesadas = new java.util.HashSet<>();
        Map<Character,java.util.Set<String>> map = new TreeMap<>();
                
        for(String palabra : palabras){
            if(!palabrasProcesadas.contains(palabra)){
                for (int i = 0; i < palabra.length(); i++) {
                    char letra = palabra.charAt(i);
                    if(!map.containsKey(letra)){
                        map.put(letra, new java.util.LinkedHashSet());                    
                    }
                    map.get(letra).add(palabra);
                }
                palabrasProcesadas.add(palabra);
            }
        }
        return map;
    }
    
    private static String mapToString(Map<Character,java.util.Set<String>> map){
        StringBuilder sb = new StringBuilder();        
        for(Map.Entry<Character, java.util.Set<String>> entry : map.entrySet()){
            sb.append(entry.getKey()).append(": ").append(entry.getValue().size()).append(" -> ").append(entry.getValue()).append("\n");
        }
        return sb.toString();
    }
}
