/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareasmaps;

/**
 *
 * @author rafa
 */
public class Persona {
    final private String dni;
    final private String nombre;
    final private int edad;
    final private int nota;

    public Persona(String dni, String nombre, int edad, int nota) {
        this.dni = dni;
        this.nombre = nombre;
        this.edad = edad;
        this.nota = nota;
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public int getNota() {
        return nota;
    }
    
    

    @Override
    public String toString() {
        return "Nombre: " + nombre + " edad: " + edad + " Dni: " + dni + " nota: " + nota;
    }    
    
}
