/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareasmaps;

/**
 *
 * @author rgcenteno
 */
public class TareaPila {
    
    public static void cambioBaseProgram(){
        int n = 0;
        int base = 0;
        do{
            System.out.println("Por favor, inserte el número a convertir");
            if(TareasMaps.teclado.hasNextInt()){
                n = TareasMaps.teclado.nextInt();
                if(n <= 0){
                    System.out.println("El número debe ser mayor que cero");
                }
            }
            TareasMaps.teclado.nextLine();
        }
        while(n <= 0);
        
        do{
            System.out.println("Por favor, inserte la base a la que desea convertirlo");
            if(TareasMaps.teclado.hasNextInt()){
                base = TareasMaps.teclado.nextInt();
                if(base < 2 || base > 9){
                    System.out.println("La base debe de tener un valor comprendido entre 2 y 9");
                }
            }
            TareasMaps.teclado.nextLine();
        }
        while(base < 2 || base > 9);
        System.out.printf("El número %d en base %d es %s\n", n, base, cambiarBase(n, base));
    }
    
    private static String cambiarBase(int num, int base){
        java.util.Stack<Integer> pila = new java.util.Stack<>();
        while(num >= base){
            int resto = num % base;
            pila.push(resto);
            num = num / base;
        }
        StringBuilder sb = new StringBuilder();
        while(!pila.empty()){
            sb.append(pila.pop());
        }
        return sb.toString();
    }
        
}
