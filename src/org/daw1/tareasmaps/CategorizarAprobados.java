/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareasmaps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author rgcenteno
 */
public class CategorizarAprobados {
    
    private static final int TAMANO = 20;
    
    public static void programa3(){
        
        List<Persona> personas = new ArrayList<>(TAMANO);
        java.util.Random random = new java.util.Random();
        for (int i = 0; i < TAMANO; i++) {
            String dni = Integer.toString(random.nextInt(9999999));
            Persona persona = new Persona(dni, "Persona" + i, random.nextInt(99), random.nextInt(11));    
            personas.add(persona);
        }
        //System.out.println(personas.stream().filter(p -> p.getNota() >= 5).collect(java.util.stream.Collectors.groupingBy(p -> p.getNota() >= 5, java.util.stream.Collectors.toList())));        
        //personas
        System.out.println(TareasMaps.mapToString(categorizar(personas)));
        
    }
    
     private static Map<Boolean, List<Persona> > categorizar(List<Persona> ps){
        Map<Boolean, List<Persona> > map = new java.util.HashMap<>();
        List<Persona> aprobados = new java.util.LinkedList<>();
        List<Persona> suspensos = new java.util.LinkedList<>();
        map.put(Boolean.TRUE, aprobados);
        map.put(Boolean.FALSE, suspensos);
        for(Persona p : ps){
            map.get(p.getNota() >= 5).add(p);                             
        }
        return map;
    }
}
