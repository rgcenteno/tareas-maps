/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareasmaps;

/**
 *
 * @author rgcenteno
 */
public class CuentaVocales {
    
    public static void programa2(){
        String texto = "";
        do{
            System.out.println("Inserte el texto a analizar");
            texto = TareasMaps.teclado.nextLine().toLowerCase();
        }
        while(texto.isEmpty());
                
        System.out.println(mapToString(contarVocales(texto)));
    }
    
    private static java.util.Map<Character, Integer> contarVocales(String texto){
        java.util.Map<Character, Integer> map = new java.util.TreeMap<>();
        map.put('a', 0);
        map.put('e', 0);
        map.put('i', 0);
        map.put('o', 0);
        map.put('u', 0);
        java.util.Set<Character> vocales = new java.util.HashSet<>();
        for (int i = 0; i < texto.length(); i++) {
            char letra = texto.charAt(i);            
            if(map.containsKey(letra) && !vocales.contains(letra)){
                vocales.add(letra);
                map.put(letra, map.get(letra) + 1);
            }
            else if(letra == ' '){
                vocales.clear();
            }
        }
        return map;
    }
    
    private static String mapToString(java.util.Map<Character, Integer> map){
        StringBuilder sb = new StringBuilder();
        for(java.util.Map.Entry<Character, Integer> entry : map.entrySet()){
            sb.append(entry.getKey()).append(": ").append(entry.getValue()).append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }
}
