/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareasmaps;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import static org.daw1.tareasmaps.TareasMaps.teclado;

/**
 *
 * @author rgcenteno
 */
public class PalabrasEmpiezan {
    
    public static void programa4(){        
        String texto = "";
        do{
            System.out.println("Por favor inserte la frase a analizar");
            texto = teclado.nextLine().toLowerCase();
        }
        while(texto.isEmpty());
        
        
        System.out.println(mapToString(contarPalabras(texto)));
    }
    
    private static Map<Character, List<String>> contarPalabras(String texto){
        String[] palabras = texto.split("\\s+");
        Map<Character, List<String>> map = new java.util.TreeMap<>();
        for(String s : palabras){
            if(!map.containsKey(s.charAt(0))){
                map.put(s.charAt(0), new LinkedList<>());
            }
            map.get(s.charAt(0)).add(s);
        }
        return map;
    }
    
    private static String mapToString(Map<Character, List<String>> map){
        StringBuilder sb = new StringBuilder();
        
        for(Map.Entry<Character, List<String>> entry : map.entrySet()){
            sb.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }
        return sb.toString();
    }
}
