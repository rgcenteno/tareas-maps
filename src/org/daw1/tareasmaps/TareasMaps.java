/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareasmaps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author rgcenteno
 */
public class TareasMaps {
    
    public static java.util.Scanner teclado;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        testMapAnidadoPares();
        teclado = new java.util.Scanner(System.in);
        String opcion = "";
        do{     
            System.out.println("************************************************");
            System.out.println("* 1. Contador de letras                        *");
            System.out.println("* 2. Palabras contienen vocales                *");
            System.out.println("* 3. Personas aprobadas y suspensas            *");
            System.out.println("* 4. Palabras empiezan letra                   *");
            System.out.println("* 5. Palabras diferentes contienen letra       *");
            System.out.println("* 6. Ejemplo maps anidados con listas          *");
            System.out.println("* 7. Ejercicio cambio de base (Stack)          *");
            System.out.println("*                                              *");
            System.out.println("* 0. Salir                                     *");
            System.out.println("************************************************");
            opcion = teclado.nextLine();
            switch(opcion){
                case "1":
                    programa1();
                    break;
                case "2":
                    CuentaVocales.programa2();
                    break;
                case "3":
                    CategorizarAprobados.programa3();
                    break;
                case "4":
                    PalabrasEmpiezan.programa4();
                    break;
                case "5":
                    PalabrasContienen.programa5();
                    break;
                case "6":
                    testMapAnidadoPares();
                    break;
                case "7":
                    TareaPila.cambioBaseProgram();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Opción inválida");
            }
        }
        while(!opcion.equals("0"));
    }
    
    private static void programa1(){
        java.util.Map<Character, Integer> map = new java.util.TreeMap<>();
        String texto = "";
        do{
            System.out.println("Inserte el texto a analizar");
            texto = teclado.nextLine().toLowerCase();
        }
        while(texto.isEmpty());
        ;
        

        
        
        System.out.println(mapToStringEj1(cuentaCaracteres(texto)));
    }
    
    private static Map<Character, Integer> cuentaCaracteres(String texto){
        java.util.Map<Character, Integer> map = new java.util.TreeMap<>();
        for (int i = 0; i < texto.length(); i++) {
            char c = texto.charAt(i);
            if(map.containsKey(c)){
                int contador = map.get(c) + 1;
                map.put(c, contador);                
            }
            else{
                map.put(c, 1);
            }
        }
        return map;
    }
    
    private static String mapToStringEj1(Map<Character, Integer> map){
        StringBuilder sb = new StringBuilder();
        for(java.util.Map.Entry<Character, Integer> entry : map.entrySet()){
            sb.append(entry.getKey()).append(": ").append(entry.getValue()).append(", ");
        }
        
        /*for(Character c : map.keySet()){
           sb.append(c).append(": ").append(map.get(c)).append(", ");
        }*/
        sb.delete(sb.length()-2, sb.length());
        return sb.toString();
    }

    private static void testMapAnidadoPares(){        
        Map<Boolean, Map<Boolean, List<Integer>>> map = new HashMap<>();
        int[] numeros = java.util.stream.IntStream.range(-100, 100).toArray();
        for(int n : numeros){
            boolean mayorQueCero = n >= 0;
            if(!map.containsKey(mayorQueCero)){
                map.put(mayorQueCero, new HashMap<>());
            }
            boolean esPar = (n % 2 == 0);
            if(!map.get(mayorQueCero).containsKey(esPar)){
                map.get(mayorQueCero).put(esPar, new LinkedList<>());
            }
            map.get(mayorQueCero).get(esPar).add(n);
        }
        System.out.println(map);
    }
    
    public static <E, V> String mapToString(Map<E, List<V>> map){
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<E, List<V>> entry : map.entrySet()){
            sb.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }
        return sb.toString();
    }
    
}
